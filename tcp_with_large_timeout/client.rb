# frozen_string_literal: true
# sharable_constant_value: literal

require 'socket'

remote = '127.0.0.1'
port = 2000
timeout = 60

begin
  puts "Connecting to #{remote} port #{port} with timeout #{timeout}."
  client = Socket.tcp(remote, port, connect_timeout: timeout)
  puts 'Connected.'

  20.times do |index|
    puts "Sending message #{index + 1}."
    client.puts "Message #{index + 1}: Hello World!"
  end

  puts 'Close socket.'
  client.close
rescue StandardError => e
  warn "Error: #{e}"
  exit 1
end
