# frozen_string_literal: true
# sharable_constant_value: literal

require 'socket'

port = 2000

puts "Starting server at port #{port}"

server = TCPServer.new port
loop do
  Thread.start(server.accept) do |socket|
    puts "New connection from #{socket.peeraddr[3]} port #{socket.peeraddr[1]}."
    while (text = socket.gets)
      puts "Received data: #{text.dump}"
    end
    puts 'No more data is received.'
  rescue StandardError => e
    warn "Error: #{e.message}"
  end
end
