
class HLTPServer

  class DoSessionOpenReject < Exception
    
    attr_reader :reason, :session_id

    def initialize(reason, session_id)
      @reason = reason
      @session_id = session_id
    end
  
  end

  class DoSessionOpenConfirm < Exception
    
    attr_reader :session_id

    def initialize(session_id)
      @session_id = session_id
    end
  
  end

  class InvalidSignatureError < StandardError; end
  class NotSupportedVersionError < StandardError; end
  class UnknownSession < StandardError; end
  class NotEstablishedSession < StandardError; end
  class OutOfSession < StandardError; end

  require 'socket'
  require 'thread'
  require 'rbnacl'
  require_relative 'Message'
  require_relative 'LatencyBuffer'
  require_relative 'IncomingBuffer'
  require_relative 'OutgoingBuffer'
  require_relative 'TCPManager'

  def initialize(host, port, forward_ports = [])
    @server = UDPSocket.new
    @server.bind host, port
    @forward_ports = forward_ports
    @sessions = {}
    @sessions_mutex = Mutex.new
    @key = RbNaCl::SigningKey.generate
    @public_key = @key.verify_key.to_s
    @tcp_manager = TCPManager.new
  end

  def run
    $logger.info "Starting server."
    @server_thread = Thread.new do
      $logger.debug "Server thread started."
      while received_data = @server.recvfrom(200)
        Thread.new(received_data) do |treceived_data|
          $logger.debug 'Received data. Calling handler.'
          receive_handler treceived_data
        rescue StandardError => e
          $logger.error "Receive handler crashed: #{e.message} - #{e.backtrace.join("\n")}"
        end
      end
    rescue StandardError => e
      $logger.error "Server crashed: #{e.message} - #{e.backtrace.join("\n")}"
    end

    @gc_thread = Thread.new do
      $logger.debug "Session gc thread started."
      session_gc
      sleep 10
    end
  end

  def join
    @server_thread&.join
    @gc_thread&.join
  end

  def new_session(remote_ip, remote_port, local_port, forward_port)
    session_id = nil

    @sessions_mutex.synchronize do
      rnd_session_id = Random.rand((1...(2**64)))
      session_id = rnd_session_id
      while @sessions.key? session_id
        session_id += 1
        if session_id >= 2**64
          session_id = 1
        end
        if session_id = rnd_session_id
          raise OutOfSession
        end
      end

      @sessions[session_id] = {}
      @sessions[session_id][:mutex] = Mutex.new
    end
    @sessions[session_id][:mutex].synchronize do
      @sessions[session_id][:outgoing_buffer] = OutgoingBuffer.new
    end
    @tcp_manager.new_server(session_id, local_port, ->() { close_session(session_id) }, ->(message) { send(session_id, message) })
    @sessions[session_id][:mutex].synchronize do
      @sessions[session_id][:status] = :open_wait
      @sessions[session_id][:last_activity] = :open_wait
      @sessions[session_id][:opening_thread] = Thread.new(session_id, forward_port, remote_ip, remote_port) { |tsession_id, tforward_port, tremote_ip, tremote_port| send_session_open_loop(tsession_id, tforward_port, tremote_ip, tremote_port) }
    end

    return session_id
  end

  def is_established?(session_id)
    @sessions_mutex.synchronize do
      return false unless @sessions.key? session_id
    end
    @sessions[session_id][:mutex].synchronize do
      return false unless @sessions[session_id][:status] == :open
    end

    return true
  end

  def send(session_id, message)
    ensure_open_session session_id

    @sessions[session_id][:mutex].synchronize do
      @sessions[session_id][:outgoing_buffer].send(message)
    end
  end

  private

  def force_delete_session(session_id)
    $logger.debug "Force delete session with id ##{session_id}."
    @sessions[session_id][:closing_thread]&.kill
    @sessions[session_id][:outgoing_thread]&.kill
    @sessions[session_id][:opening_thread]&.kill
    @sessions[session_id][:latency_thread]&.kill
    @sessions.delete session_id
  end

  def graceful_delete_session(session_id)
    $logger.debug "Graceful delete session with id ##{session_id}."
    @sessions[session_id].mutex do
      force_delete_session session_id
    end
  end

  def get_session_endpoint(session_id)
    remote_ip = nil
    remote_port = nil

    @sessions[session_id][:mutex].synchronize do
      remote_ip = @sessions[session_id][:remote_ip]
      remote_port = @sessions[session_id][:remote_port]
    end

    # $logger.debug "Endpoint for session ##{session_id} is #{remote_ip}:#{remote_port}."
    return remote_ip, remote_port
  end

  def session_gc
    $logger.debug 'Do session gc.'
    @sessions_mutex.synchronize do
      @sessions.each do |session_id, session|
        if session[:status] == :close_wait && (Time.now - session[:close_timepoint]) > 2*60 ||
          (session[:status] == :open) && (Time.now - session[:last_activity] > [2*60, session[:latency_buffer].get_latency].max)
          $logger.debug "Found outdated session ##{session_id}."
          graceful_delete_session session_id
        end
      end
    end
  end

  def send_session_open_loop(session_id, port, remote_ip, remote_port)
    loop do
      send_session_open(session_id, port, remote_ip, remote_port)
      sleep 2
    end
  end

  def close_session(session_id)
    $logger.debug "Close session ##{session_id}."
    @sessions[session_id][:mutex].synchronize do
      rtt = @sessions[session_id][:latency_buffer]&.get_latency
      rtt ||= 1
      closing_thread = Thread.new(session_id, rtt) do |tsession_id, trtt|
        loop do
          send_session_close(session_id)
          sleep trtt
        end
      end

      @sessions[session_id][:status] = :close_wait
      @sessions[session_id][:close_timepoint] = Time.now
      @sessions[session_id][:closing_thread] = closing_thread
      @sessions[session_id][:outgoing_thread]&.kill
    end
  end

  def receive_handler(received_data)
    message = Message.from_msgpack(received_data[0])
    $logger.debug "Received message type=#{message['TYPE']}, version=#{message['VERSION']} session_id=#{message['SESSION_ID']}."

    message.validate
    remote_ip = received_data[1][3]
    remote_port = received_data[1][1]

    if message['VERSION'] != 1
      raise NotSupportedVersionError
    end

    case message['TYPE']
    when 'SESSION_OPEN'
      session_open_handler(message, remote_ip, remote_port)
    when 'SESSION_OPEN_REJECT'
      session_open_reject_handler(message, remote_ip, remote_port)
    when 'SESSION_OPEN_CONFIRM'
      session_open_confirm_handler(message, remote_ip, remote_port)
    when 'SESSION_CLOSE'
      session_close_handler(message, remote_ip, remote_port)
    when 'LATENCY_FORWARD_MESSAGE'
      latency_forward_handler(message, remote_ip, remote_port)
    when 'LATENCY_RETURN_MESSAGE'
      latency_return_handler(message, remote_ip, remote_port)
    when 'MESSAGE'
      message_handler(message, remote_ip, remote_port)
    when 'MESSAGE_ACKNOWLEDGE'
      message_acknowledge_handler(message, remote_ip, remote_port)
    else
      raise 'Unknown message type. Validation is broken.'
    end
  rescue MessageError => e
    $logger.error "Received invalid message: #{e.message} - #{e.backtrace&.first}"
  rescue UnknownSession => e
    $logger.info "Received message for unknown session."
  rescue StandardError => e
    $logger.error "Unknown error: #{e.message} - #{e.backtrace.join("\n")}"
  end

  def ensure_open_session(session_id)
    raise UnknownSession unless @sessions.key? session_id
    @sessions[session_id][:mutex].synchronize do
      raise NotEstablishedSession unless @sessions[session_id][:status] == :open
    end
  end

  def session_open_handler(message, remote_ip, remote_port)
    session_id = message['SESSION_ID']

    raise DoSessionOpenReject.new('TCP_PORT_NOT_ALLOWED', session_id) unless @forward_ports.include? message['TO_PORT']

    $logger.debug 'Verifying session open message.'
    public_key = RbNaCl::VerifyKey.new(message['PUBLIC_KEY'])
    message.verify(public_key)

    @sessions_mutex.synchronize do
      if @sessions.key?(session_id)
        @sessions[session_id][:mutex].synchronize do
          if @sessions[session_id][:public_key].to_s == message['PUBLIC_KEY'] && @sessions[session_id][:status] == :open
            $logger.debug "Session ##{session_id} already open. Send confirmation again."
            raise DoSessionOpenConfirm.new(session_id)
          else
            $logger.debug "Session ID ##{session_id} already in use."
            raise DoSessionOpenReject.new('SESSION_ID_IN_USE', session_id)
          end
        end
      end

      @sessions[session_id] = {}
      @sessions[session_id][:mutex] = Mutex.new
      $logger.debug "Session ##{session_id} created."
    end
    @tcp_manager.new_client(session_id, message['TO_PORT'], ->() { close_session(session_id) }, ->(message) { send(session_id, message) })
    @sessions[session_id][:mutex].synchronize do
      $logger.debug "Setting initial variables for session ##{session_id}."
      @sessions[session_id][:latency_buffer] = LatencyBuffer.new
      @sessions[session_id][:public_key] = public_key
      @sessions[session_id][:status] = :open
      @sessions[session_id][:remote_ip] = remote_ip
      @sessions[session_id][:remote_port] = remote_port
      @sessions[session_id][:close_timepoint] = nil
      @sessions[session_id][:incoming_buffer] = IncomingBuffer.new ->(payload) { @tcp_manager.forward(session_id, payload) }
      @sessions[session_id][:outgoing_buffer] = OutgoingBuffer.new
      @sessions[session_id][:outgoing_thread] = Thread.new(session_id) { |tsession_id| send_messages_loop(tsession_id) }
      @sessions[session_id][:latency_thread] = Thread.new(session_id) { |tsession_id| send_latency_forward_message_loop(tsession_id) }
      @sessions[session_id][:last_activity] = Time.new
      raise DoSessionOpenConfirm.new(session_id)
    end
  rescue DoSessionOpenConfirm => e
    send_session_open_confirm e.session_id
  rescue DoSessionOpenReject => e
    send_session_open_reject e.session_id, e.reason, remote_ip, remote_port
  rescue FailedToVerifyMessageError => e
    warn "Message with invalid signature received: #{e.message} - #{e.backtrace&.first}"
  end

  def session_open_confirm_handler(message, remote_ip, remote_port)
    session_id = message['SESSION_ID']

    @sessions_mutex.synchronize do
      raise UnknownSession unless @sessions.key? session_id
    end

    public_key = RbNaCl::VerifyKey.new(message['PUBLIC_KEY'])
    message.verify(public_key)

    @sessions[session_id][:mutex].synchronize do
      return if @sessions[session_id][:status] == :open

      $logger.debug "Setting initial variables for session ##{session_id}."
      @sessions[session_id][:opening_thread].kill
      @sessions[session_id][:latency_buffer] = LatencyBuffer.new
      @sessions[session_id][:public_key] = public_key
      @sessions[session_id][:status] = :open
      @sessions[session_id][:remote_ip] = remote_ip
      @sessions[session_id][:remote_port] = remote_port
      @sessions[session_id][:close_timepoint] = nil
      @sessions[session_id][:incoming_buffer] = IncomingBuffer.new ->(payload) { @tcp_manager.forward(session_id, payload) }
      @sessions[session_id][:outgoing_thread] = Thread.new(session_id) { |tsession_id| send_messages_loop(tsession_id) }
      @sessions[session_id][:latency_thread] = Thread.new(session_id) { |tsession_id| send_latency_forward_message_loop(tsession_id) }
      @sessions[session_id][:last_activity] = Time.new
    end
  end

  def session_open_reject_handler(message, remote_ip, remote_port)
    session_id = message['SESSION_ID']

    public_key = RbNaCl::VerifyKey.new(message['PUBLIC_KEY'])
    message.verify(public_key)

    unless @sessions.key? session_id
      $logger.info 'Received session open reject for non-existing session.'
      return
    end

    $logger.warning "Received session open reject for session ##{session_id} with reason #{message['REASON'].to_s.downcase.tr('_', '')}"
    graceful_delete_session session_id
  rescue StandardError => e
    warn "Failed handle session open reject: #{e.message} - #{e.backtrace&.first}"
  end

  def latency_forward_handler(message, remote_ip, remote_port)
    session_id = message['SESSION_ID']
    ensure_open_session(session_id)
    message.verify(@sessions[session_id][:public_key])

    @sessions[session_id][:mutex].synchronize do
      @sessions[session_id][:remote_ip] = remote_ip
      @sessions[session_id][:remote_port] = remote_port
      @sessions[session_id][:last_activity] = Time.new
    end

    send_latency_return_message(session_id, message['ID'])
  rescue FailedToVerifyMessageError => e
    warn "Message with invalid signature received: #{e.message} - #{e.backtrace&.first}"
  end

  def latency_return_handler(message, remote_ip, remote_port)
    session_id = message['SESSION_ID']
    ensure_open_session(session_id)
    message.verify(@sessions[session_id][:public_key])

    @sessions[session_id][:mutex].synchronize do
      @sessions[session_id][:remote_ip] = remote_ip
      @sessions[session_id][:remote_port] = remote_port
      @sessions[session_id][:last_activity] = Time.new

      @sessions[session_id][:latency_buffer].receive_id_handler message['ID']
    end
  rescue FailedToVerifyMessageError => e
    warn "Message with invalid signature received: #{e.message} - #{e.backtrace&.first}"
  end

  def message_handler(message, remote_ip, remote_port)
    session_id = message['SESSION_ID']
    ensure_open_session(session_id)
    message.verify(@sessions[session_id][:public_key])

    @sessions[session_id][:mutex].synchronize do
      @sessions[session_id][:remote_ip] = remote_ip
      @sessions[session_id][:remote_port] = remote_port
      @sessions[session_id][:last_activity] = Time.new

      @sessions[session_id][:incoming_buffer].receive_message_handler message['ID'], message['TIMESTAMP'], message['PAYLOAD'], @sessions[session_id][:latency_buffer].get_latency
    end

    send_message_acknowledge(session_id, message['ID'])
  rescue FailedToVerifyMessageError => e
    warn "Message with invalid signature received: #{e.message} - #{e.backtrace&.first}"
  end

  def message_acknowledge_handler(message, remote_ip, remote_port)
    session_id = message['SESSION_ID']
    ensure_open_session(session_id)
    message.verify(@sessions[session_id][:public_key])

    @sessions[session_id][:mutex].synchronize do
      @sessions[session_id][:remote_ip] = remote_ip
      @sessions[session_id][:remote_port] = remote_port
      @sessions[session_id][:last_activity] = Time.new

      @sessions[session_id][:outgoing_buffer].acknowledge_message message['ID'], message['TIMESTAMP'], @sessions[session_id][:latency_buffer].get_latency
    end
  rescue FailedToVerifyMessageError => e
    warn "Message with invalid signature received: #{e.message} - #{e.backtrace&.first}"
  end

  def session_close_handler(message, remote_ip, remote_port)
    session_id = message['SESSION_ID']
    message.verify(@sessions[session_id][:public_key])

    @sessions[session_id][:mutex].synchronize do
      @sessions[session_id][:remote_ip] = remote_ip
      @sessions[session_id][:remote_port] = remote_port

      if @sessions[session_id][:status] == :open
        @sessions[session_id][:status] = :close_wait
        @sessions[session_id][:close_timepoint] = Time.now
      end
    end

    @tcp_manager.close_client(session_id)
    send_session_close_acknowledge(session_id)
  rescue FailedToVerifyMessageError => e
    warn "Message with invalid signature received: #{e.message} - #{e.backtrace&.first}"
  end

  def send_latency_forward_message(session_id)
    message = Message.new
    message['VERSION'] = 1
    message['TYPE'] = 'LATENCY_FORWARD_MESSAGE'
    message['SESSION_ID'] = session_id

    @sessions[session_id][:mutex].synchronize do
      message['ID'] = @sessions[session_id][:latency_buffer].get_packet_id
    end

    packed_message = message.sign_and_pack(@key)
    remote_ip, remote_port = get_session_endpoint session_id
    @server.send packed_message, 0, remote_ip, remote_port
  rescue StandardError => e
    warn "Failed to send latency forward message: #{e.message} - #{e.backtrace&.first}"
  end

  def send_latency_forward_message_loop(session_id)
    loop do
      send_latency_forward_message(session_id)
      rtt = nil
      @sessions[session_id][:mutex].synchronize do
        rtt = @sessions[session_id][:latency_buffer].get_latency
      end
      sleep rtt * 2
    end
  end

  def send_message_acknowledge(session_id, id)
    $logger.debug "Sending message acknowledge for session ##{session_id}, message ##{id}."
    message = Message.new
    message['VERSION'] = 1
    message['TYPE'] = 'MESSAGE_ACKNOWLEDGE'
    message['SESSION_ID'] = session_id

    message['ID'] = id
    message['TIMESTAMP'] = Time.now

    packed_message = message.sign_and_pack(@key)
    remote_ip, remote_port = get_session_endpoint session_id
    @server.send packed_message, 0, remote_ip, remote_port
  rescue StandardError => e
    warn "Failed to send message acknowledge: #{e.message} - #{e.backtrace&.first}"
  end

  def send_latency_return_message(session_id, id)
    message = Message.new
    message['VERSION'] = 1
    message['TYPE'] = 'LATENCY_RETURN_MESSAGE'
    message['SESSION_ID'] = session_id
    message['ID'] = id

    packed_message = message.sign_and_pack(@key)
    remote_ip, remote_port = get_session_endpoint session_id
    @server.send packed_message, 0, remote_ip, remote_port
  rescue StandardError => e
    warn "Failed to send session close: #{e.message} - #{e.backtrace&.first}"
  end

  def send_session_close(session_id)
    message = Message.new
    message['VERSION'] = 1
    message['TYPE'] = 'SESSION_CLOSE'
    message['SESSION_ID'] = session_id

    packed_message = message.sign_and_pack(@key)
    remote_ip, remote_port = get_session_endpoint session_id
    @server.send packed_message, 0, remote_ip, remote_port
  rescue StandardError => e
    warn "Failed to send session close: #{e.message} - #{e.backtrace&.first}"
  end

  def send_session_close_acknowledge(session_id)
    message = Message.new
    message['VERSION'] = 1
    message['TYPE'] = 'SESSION_CLOSE_ACKNOWLEDGE'
    message['SESSION_ID'] = session_id

    packed_message = message.sign_and_pack(@key)
    remote_ip, remote_port = get_session_endpoint session_id
    @server.send packed_message, 0, remote_ip, remote_port
  rescue StandardError => e
    warn "Failed to send session close acknowledge: #{e.message} - #{e.backtrace&.first}"
  end

  def send_session_open_confirm(session_id)
    $logger.debug "Sending session open confirm for session ##{session_id}."

    message = Message.new
    message['VERSION'] = 1
    message['TYPE'] = 'SESSION_OPEN_CONFIRM'
    message['SESSION_ID'] = session_id

    message['PUBLIC_KEY'] = @public_key
    packed_message = message.sign_and_pack(@key)
    remote_ip, remote_port = get_session_endpoint(session_id)
    @server.send packed_message, 0, remote_ip, remote_port
  rescue StandardError => e
    warn "Failed to send session open confirm: #{e.message} - #{e.backtrace&.first}"
  end

  def send_session_open_reject(session_id, reason, remote_ip, remote_port)
    $logger.debug "Send session open reject to #{remote_ip}:#{remote_port} with reasion #{reason}."
    message = Message.new
    message['VERSION'] = 1
    message['TYPE'] = 'SESSION_OPEN_REJECT'
    message['SESSION_ID'] = session_id

    message['PUBLIC_KEY'] = @public_key
    message['REASON'] = reason
    packed_message = message.sign_and_pack(@key)
    @server.send packed_message, 0, remote_ip, remote_port
  rescue StandardError => e
    warn "Failed to send session open reject: #{e.message} - #{e.backtrace&.first}"
  end

  def send_messages_loop(session_id)
    loop do
      send_messages(session_id)
      rtt = nil
      @sessions[session_id][:mutex].synchronize do
        rtt = @sessions[session_id][:latency_buffer].get_latency
      end
      sleep rtt
    end
  end

  def send_messages(session_id)
    messages = []
    @sessions[session_id][:mutex].synchronize do
      $logger.debug "Sending outgoing messages for session ##{session_id}."
      messages = @sessions[session_id][:outgoing_buffer].get_messages
    end
    messages.each do |message|
      send_message(session_id, message[0], message[1])
    end
  end

  def send_message(session_id, id, payload)
    $logger.debug "Send message over session ##{session_id}."
    message = Message.new
    message['VERSION'] = 1
    message['TYPE'] = 'MESSAGE'
    message['SESSION_ID'] = session_id

    message['ID'] = id
    message['PAYLOAD'] = payload
    message['TIMESTAMP'] = Time.now

    packed_message = message.sign_and_pack(@key)
    remote_ip, remote_port = get_session_endpoint session_id
    @server.send packed_message, 0, remote_ip, remote_port
  end

  def send_session_open(session_id, forward_port, remote_ip, remote_port)
    $logger.debug "Send session open for session ##{session_id}."
    message = Message.new
    message['VERSION'] = 1
    message['TYPE'] = 'SESSION_OPEN'
    message['SESSION_ID'] = session_id

    message['TO_PORT'] = forward_port
    message['PUBLIC_KEY'] = @public_key

    packed_message = message.sign_and_pack(@key)
    @server.send packed_message, 0, remote_ip, remote_port
  end

end
