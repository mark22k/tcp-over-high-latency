
require 'msgpack'
require 'rbnacl'

MessagePack::DefaultFactory.register_type(
  MessagePack::Timestamp::TYPE,
  Time,
  packer: MessagePack::Time::Packer,
  unpacker: MessagePack::Time::Unpacker
)

key = RbNaCl::SigningKey.generate

def create_packet(size, key)
    inner_message = {
        'VERSION' => 1,
        'TYPE' => 'MESSAGE',
        'SESSION_ID' => 2**64 - 1,
        'ID' => 2**64 - 1,
        'TIMESTAMP' => Time.now,
        'PAYLOAD' => "\t" * size
    }.to_msgpack
    signature = key.sign(inner_message)
    message = [inner_message, signature].to_msgpack

    return message.length
end

for payload_size in 1..200
    message_size = create_packet(payload_size, key)
    puts "#{payload_size}\t#{message_size}"
end
