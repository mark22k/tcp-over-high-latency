
require 'logger'
require 'msgpack'
require_relative 'Server'

MessagePack::DefaultFactory.register_type(
  MessagePack::Timestamp::TYPE,
  Time,
  packer: MessagePack::Time::Packer,
  unpacker: MessagePack::Time::Unpacker
)

$logger = Logger.new(STDOUT)
$logger.level = Logger::DEBUG

port = 3794
client = HLTPServer.new '10.115.253.197', port
client.run
sid = client.new_session '10.115.180.153', 3793, 1234, 1234

until client.is_established? sid
  puts "Waiting for session to be established..."
  sleep 1
end

client.join
