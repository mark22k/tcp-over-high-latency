
require 'logger'
require 'msgpack'
require_relative 'Server'

MessagePack::DefaultFactory.register_type(
  MessagePack::Timestamp::TYPE,
  Time,
  packer: MessagePack::Time::Packer,
  unpacker: MessagePack::Time::Unpacker
)

$logger = Logger.new(STDOUT)
$logger.level = Logger::DEBUG

port = 3793
server = HLTPServer.new '10.115.180.153', port, [1234]
server.run
server.join
