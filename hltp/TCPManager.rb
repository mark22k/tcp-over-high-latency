
class TCPManager

    def initialize(host = 'localhost')
        @host = host
        @clients = {}
    end

    def new_client(session_id, port, close_handler, send_handler)
        begin
            socket = TCPSocket.new(@host, port)
            new_socket(session_id, socket, close_handler, send_handler)
        rescue StandardError => e
            raise e
            close_handler.()
        end
    end

    def new_socket(session_id, socket, close_handler, send_handler)
        @clients[session_id] = {}
        @clients[session_id][:socket] = socket
        @clients[session_id][:close_handler] = close_handler
        @clients[session_id][:send_thread] = Thread.new(session_id, socket, send_handler) do |tsession_id, tsocket, tsend_handler|
            loop do
                data = tsocket.recvfrom(40)[0]
                tsend_handler.(data)
            rescue StandardError => e
                raise e
                return
                close_client(tsession_id)
            end
        end
    end

    def forward(session_id, payload)
        $logger.debug "Session ##{session_id} forward tcp data."
        @clients[session_id][:socket].write payload
    end

    def close_client(session_id)
        @clients[session_id][:send_thread].kill
        @clients[session_id][:socket].close
    end

    def new_server(session_id, port, close_handler, send_handler)
        begin
            socket = TCPServer.new(@host, port).accept
            new_socket(session_id, socket, close_handler, send_handler)
        rescue StandardError => e
            raise e
            close_handler.()
        end
    end

end
