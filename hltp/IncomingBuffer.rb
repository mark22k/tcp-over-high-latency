
class IncomingBuffer

    class AttackProtection < StandardError; end
    class ReplayAttackProtectionError < AttackProtection; end

    def initialize(receive_handler)
        @buffer = {}
        @buffer_mutex = Mutex.new
        @current_id = 1
        @receive_handler = receive_handler
    end

    def receive_message_handler(id, timestamp, payload, rtt)
        if (Time.now - timestamp) > 2 * 60
            raise ReplayAttackProtectionError, 'Packet arrives too late and therefore cannot be verified.'
        end
        @buffer_mutex.synchronize do
            if @buffer.key? id
                if @buffer[id] != payload
                    raise AttackProtection, 'A packet with an existing ID has been received which is valid but has different content.'
                end
            end
            @buffer[id] = payload if id >= @current_id
        end

        recived_payload = get_next_payload
        if recived_payload
            @receive_handler.(recived_payload)
        end

        pp @buffer
        $logger.debug "#{@buffer.length} packets pending. Current ID is #{@current_id}."
    end

    def get_next_payload
        @buffer_mutex.synchronize do
            if @buffer.key? @current_id
                payload = @buffer[@current_id]
                @buffer.delete @current_id
                @current_id += 1
                if @current_id > 2**64 - 1
                    @current_id = 1
                end

                return payload
            end
        end

        return nil
    end

end
