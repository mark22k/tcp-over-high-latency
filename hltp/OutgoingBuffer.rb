
class OutgoingBuffer

    class AttackProtection < StandardError; end
    class ReplayAttackProtectionError < AttackProtection; end

    def initialize
        @buffer = {}
        @buffer_mutex = Mutex.new
        @current_id = 1
    end

    def get_messages
        @buffer_mutex.synchronize do
            messages = @buffer.first 4
            max = messages[0].to_a[0]
            messages.reject { |message| message[0] < max }

            return messages
        end
    end

    def acknowledge_message(id, timestamp, rtt)
        if (Time.now - timestamp) > 2 * 60
            raise ReplayAttackProtectionError, 'Packet arrives too late and therefore cannot be verified.'
        end

        @buffer_mutex.synchronize do
            @buffer.delete id
        end
    end

    def send(payload)
        @buffer_mutex.synchronize do
            @buffer[@current_id] = payload

            @current_id += 1
            if @current_id > 2**64 - 1
                @current_id = 1
            end
        end
    end

end
