
class MessageError < StandardError; end
class InvalidMessageError < MessageError; end
class FailedToUnpackMessageError < MessageError; end
class FailedToVerifyMessageError < MessageError; end

class Message

  require 'msgpack'
  require 'rbnacl'

  attr_accessor :message, :signature

  def self.from_msgpack(str)
    message, signature = MessagePack.unpack(str)
    return Message.new(MessagePack.unpack(message), signature)
  rescue MessagePack::MalformedFormatError => e
    raise FailedToUnpackMessageError, e.message
  end

  def initialize(message = {}, signature = "")
    @message = message
    @signature = signature
  end

  def to_msgpack
    message.to_msgpack
  end

  def sign_and_pack(key)
    packed_message = to_msgpack
    signature = key.sign(packed_message)
    return [packed_message, signature].to_msgpack
  end

  def verify(key)
    begin
      key.verify signature, @message.to_msgpack
    rescue RbNaCl::BadSignatureError => e
      raise FailedToVerifyMessageError, e.message
    end
  end

  def [](key)
    @message[key.to_s.upcase]
  end

  def []=(key, value)
    @message[key.to_s.upcase] = value
  end

  def validate
    raise InvalidMessageError, 'Message must be a map-object.' unless @message.is_a? Hash
    raise InvalidMessageError, 'Signature must be a string.' unless @signature.is_a? String

    validate_attribute('VERSION', Integer)
    validate_attribute('TYPE', String)
    validate_enum('TYPE', %w[SESSION_OPEN SESSION_OPEN_CONFIRM SESSION_OPEN_REJECT SESSION_CLOSE SESSION_CLOSE_ACKNOWLEDGE LATENCY_FORWARD_MESSAGE LATENCY_RETURN_MESSAGE MESSAGE MESSAGE_ACKNOWLEDGE])
    validate_attribute('SESSION_ID', Integer)
    validate_integer('SESSION_ID', 2**64 - 1)

    case @message['TYPE']
    when 'SESSION_OPEN'
      validate_attribute('TO_PORT', Integer)
      validate_integer('TO_PORT', 2**16 - 1)
      validate_attribute('PUBLIC_KEY', String)
      validate_string_length('PUBLIC_KEY', 32)
    when 'SESSION_OPEN_CONFIRM'
      validate_attribute('PUBLIC_KEY', String)
      validate_string_length('PUBLIC_KEY', 32)
    when 'SESSION_OPEN_REJECT'
      validate_attribute('REASON', String)
      validate_enum('REASON', %w[SESSION_ID_IN_USE SESSION_ID_INVALID TCP_PORT_NOT_ALLOWED OTHER])
    when 'LATENCY_FORWARD_MESSAGE'
      validate_attribute('ID', Integer)
      validate_integer('ID', 2**64 - 1)
    when 'LATENCY_RETURN_MESSAGE'
      validate_attribute('ID', Integer)
      validate_integer('ID', 2**64 - 1)
    when 'MESSAGE'
      validate_attribute('ID', Integer)
      validate_integer('ID', 2**64 - 1)
      validate_attribute('TIMESTAMP', Time)
      validate_attribute('PAYLOAD', String)
    when 'MESSAGE_ACKNOWLEDGE'
      validate_attribute('ID', Integer)
      validate_integer('ID', 2**64 - 1)
    end
  end

  def validate_attribute(name, type)
    raise InvalidMessageError, "Message must have #{name} attribute." unless @message.key? name
    raise InvalidMessageError, "#{name} attribute must be a #{type}." unless @message[name].is_a? type
  end

  def validate_enum(name, values)
    raise InvalidMessageError, "#{name} has a invalid value." unless values.include? @message[name]
  end

  def validate_integer(name, max)
    raise InvalidMessageError, "#{name} is too big." unless @message[name] <= max
  end

  def validate_string_length(name, length)
    raise InvalidMessageError, "#{name} has a invalid length." unless @message[name].length == length
  end
end
